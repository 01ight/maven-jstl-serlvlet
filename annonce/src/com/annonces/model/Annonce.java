package com.annonces.model;

import java.sql.Date;

public class Annonce {
	
	private Integer id  ; 
	private String  title ; 
	private String  description ; 
	private String  adress ; 
	private String  email  ; 
	private Date date  ; 
	
	public  Annonce() {
		
	}
	
	public  Annonce (Integer id , String  title,  String  description , String adress, String email, Date date)
	{
		this.id  = id ; 
		this .title = title ; 
		this .description = description ; 
		this.adress = adress ; 
		this.email = email ; 
		this.date = date  ;
		
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	
	
	
}
