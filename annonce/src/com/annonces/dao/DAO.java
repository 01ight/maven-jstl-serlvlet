package com.annonces.dao;

import java.sql.Connection;
import java.util.ArrayList;

public abstract  class DAO  <T>{
	
	protected Connection  connect = null ; 
	public DAO () {
		
	}
	public DAO (Connection  con) 
	{
		this.connect =  con ; 
	}

	public abstract boolean  create (T obj )  ; 
	
	public abstract boolean  delete( int id) ; 
	public abstract boolean  update(T obj ) ; 
	
	public abstract T  find(int id) ;
	
	public abstract ArrayList<T> list() ;
		
}
