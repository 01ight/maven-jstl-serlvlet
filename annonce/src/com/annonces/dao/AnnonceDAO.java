package com.annonces.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.annonces.model.Annonce;;

public class AnnonceDAO extends DAO<Annonce>{

	public AnnonceDAO () {
		
	}
	public  AnnonceDAO ( Connection  connection) {
		super(connection) ;
	}
	
	@Override
	public boolean create(Annonce obj) {
		// TODO Auto-generated method stub

		try {
			
	
				PreparedStatement prepare  = this.connect
											     .prepareStatement("INSERT INTO  annonces ( title , description, adress, email, date)"
											     		+ "VALUES (?,?,?, ?,?)") ; 
				
				prepare.setString(1, obj.getTitle());
				prepare.setString(2, obj.getDescription());
				prepare.setString(3, obj.getAdress());
				prepare.setString(4, obj.getEmail());
				prepare.setDate(5,  obj.getDate());
			
				prepare.executeUpdate() ; 
		}
		catch (SQLException  e )
		{
			e.printStackTrace();
		}
		return true ; 
	}

	@Override
	public boolean delete( int id ) {
		try {
				
			this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
						.executeQuery("DELETE FROM  annonces WHERE id = "+ id) ;
		}
		catch (SQLException  e )
		{
			e.printStackTrace();
		}
		return  true ;
	}

	@Override
	public boolean update(Annonce obj ) {
		
		try {
			
			
			PreparedStatement prepare  = this.connect
										     .prepareStatement("UPDATE annonces SET title = ? , description = ?, adress = ?, email = ? WHERE id = ?") ; 
			
			prepare.setString(1, obj.getTitle());
			prepare.setString(2, obj.getDescription());
			prepare.setString(3, obj.getAdress());
			prepare.setString(4, obj.getEmail());
			prepare.setLong(5, obj.getId());
			
			prepare.executeUpdate() ; 
	}
	catch (SQLException  e )
	{
		e.printStackTrace();
	}
		return  true ; 
	}

	@Override
	public Annonce find(int id) {
		
		Annonce Annonce = new Annonce() ;
		
		try {
				ResultSet result = this.connect
										.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
										.executeQuery("SELECT * FROM annonces WHERE id = " + id) ; 
				if(result.first() )
				{
					
					Annonce = new Annonce(result.getInt("id"), result.getString("title"), 
							result.getString("description"),
							result.getString("adress"), 
							result.getString("email") , 
							result.getDate("date")  ) ;
					
					
				}
			
		}catch (SQLException e)
		{
			e.printStackTrace();
		}
		return  Annonce  ; 
	}

	@Override
	public ArrayList<Annonce> list() {
	
		ArrayList <Annonce>  AnnonceList  = new ArrayList<Annonce>() ;
		Annonce  Annonce = new Annonce() ; 
		try {
			ResultSet result = this.connect
					.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
					.executeQuery("SELECT * FROM annonces  ") ; 
			while (result.next() )
			{
				Annonce = new Annonce(result.getInt("id"), result.getString("title"), 
						result.getString("description"),
						result.getString("adress"), result.getString("email") , 
						result.getDate("date")  ) ;
				AnnonceList.add(Annonce) ; 
			}
		}
		catch (SQLException  e) 
		{
			e.printStackTrace();
		}
		return AnnonceList  ; 
		
	}

	

}
