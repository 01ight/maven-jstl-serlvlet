package com.annonces.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.annonces.dao.AnnonceDAO; 
import com.annonces.dao.ConnectionDB;
import com.annonces.model.Annonce;
/**
 * Servlet implementation class listesAnnoncesServlet
 */
@WebServlet("/annonces")
public class listesAnnoncesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AnnonceDAO annonces =null ; 
		ArrayList<Annonce> annoncesList = new ArrayList<Annonce>() ; 
		try {
			 annonces = new AnnonceDAO(ConnectionDB.getInstance()) ;
			 annoncesList = annonces.list() ;
			 for (Annonce item : annoncesList) {   
				    System.out.println(item.getTitle() + " " + item.getDescription());
				}
		} catch (ClassNotFoundException e) {
			// 
			e.printStackTrace();
		} 
		request.setAttribute("annoncesList", annoncesList );
		request.getRequestDispatcher("/WEB-INF/pages/annonces.jsp").forward(request,response);
	}
}
