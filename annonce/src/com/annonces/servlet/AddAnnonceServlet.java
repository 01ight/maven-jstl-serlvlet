package com.annonces.servlet;

import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.annonces.dao.AnnonceDAO;
import com.annonces.dao.ConnectionDB;
import com.annonces.model.Annonce;

/**
 * Servlet implementation class AddAnnonceServlet
 */
@WebServlet("/ajouterAnnonce")
public class AddAnnonceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.getRequestDispatcher("/WEB-INF/pages/AnnonceAdd.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Annonce newAnnonce = new Annonce()  ; 
		newAnnonce.setTitle(request.getParameter("title"));
		newAnnonce.setDescription(request.getParameter("description"));
		newAnnonce.setAdress(request.getParameter("adress"));
		newAnnonce.setEmail(request.getParameter("email"));
		
		java.util.Date utilDate = new java.util.Date();
		java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		
		newAnnonce.setDate(sqlDate);
		 AnnonceDAO  annonce = null ; 
		 
		try {
			annonce = new AnnonceDAO(ConnectionDB.getInstance()) ;
			annonce.create(newAnnonce) ; 
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(newAnnonce.toString()  ); 
		
		response.sendRedirect("annonces");
	}

}
