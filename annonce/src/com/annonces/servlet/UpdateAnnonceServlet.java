package com.annonces.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.annonces.dao.AnnonceDAO;
import com.annonces.dao.ConnectionDB;
import com.annonces.model.Annonce;

/**
 * Servlet implementation class UpdateAnnonceServlet
 */
@WebServlet("/modifierAnnonce")
public class UpdateAnnonceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	private int id  ;
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AnnonceDAO annonces =null ; 
		Annonce a  = null ; 
		ArrayList<Annonce> annoncesList = new ArrayList<Annonce>() ; 
		try {
			 annonces = new AnnonceDAO(ConnectionDB.getInstance()) ;
			 id = Integer.parseInt(request.getParameter("id")) ;
			a =  annonces.find(id) ;
			
			
		} catch (ClassNotFoundException e) {
			// 
			e.printStackTrace();
		} 
	request.setAttribute("annonce", a );
	request.getRequestDispatcher("/WEB-INF/pages/AnnonceUpdate.jsp").forward(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Annonce updatedAnnonce = new Annonce()  ; 
		updatedAnnonce.setTitle(request.getParameter("title"));
		updatedAnnonce.setDescription(request.getParameter("description"));
		updatedAnnonce.setAdress(request.getParameter("adress"));
		updatedAnnonce.setEmail(request.getParameter("email"));
		
		 
		updatedAnnonce.setId(id);
		
	
		 AnnonceDAO  annonce = null ; 
		 
		try {
			annonce = new AnnonceDAO(ConnectionDB.getInstance()) ;
			annonce.update(updatedAnnonce) ; 
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.sendRedirect("annonces");
	}

}
