package com.annonces.servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.annonces.dao.AnnonceDAO;
import com.annonces.dao.ConnectionDB;
import com.annonces.model.Annonce;

/**
 * Servlet implementation class DeleteAnnonceServlet
 */
@WebServlet("/supprimerAnnonce")
public class DeleteAnnonceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AnnonceDAO annonces =null ; 
	 
		try {
			 annonces = new AnnonceDAO(ConnectionDB.getInstance()) ;
			 Integer id = Integer.parseInt(request.getParameter("id")) ;
			boolean  e =  annonces.delete(id) ;
			
		} catch (ClassNotFoundException e) {
			// 
			e.printStackTrace();
		} 
		response.sendRedirect("annonces");
	}

}
