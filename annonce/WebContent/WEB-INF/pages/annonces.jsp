<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
  <%@ page import="java.util.ArrayList" %>
  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Liste des annonces</title>
<link  rel="stylesheet"  type= "text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
	  <h1> liste des annonces : </h1>
	  
		 <table class="table">
		 	
		     <c:forEach var="annonce" items="${annoncesList}">
		     <tr>
		     	<td>
       		     <c:out value="${annonce.getTitle() }" />
       		      (<c:out value="${ annonce.getEmail()}" /> )
       		     </td>
       		     
       		     <td>
       		         <c:out value="${annonce.getDate()}" />
       		 		<a href="modifierAnnonce?id=${annonce.getId()}"><span class="glyphicon glyphicon-pencil"></span></a>
       		 		<a href="supprimerAnnonce?id=${annonce.getId()}"><span class="glyphicon glyphicon-remove"></span></a>
       		     </td>
       		    
       		  </tr>
   			 </c:forEach>
	 	 </table>
	 	 <a href="ajouterAnnonce" class="btn  btn-primary "> Add an Item</a>
	</div>

</body>
</html>