<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ajouter une annonce</title>
<link  rel="stylesheet"  type= "text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
</head>
<body>
<div class= "container">
<h1>Ajouter une annonce </h1>
<form  action = "ajouterAnnonce"  method="POST">
  <div class="form-group">
    <label for="exampleInputEmail1">Title</label>
    <input type="text" class="form-control" id="title"  name ="title" placeholder="enter title">
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Description</label>
    <textarea  class="form-control" id="description" placeholder="enter description" name="description"> </textarea>
  </div>
   <div class="form-group">
    <label for="exampleInputEmail1">Adress</label>
    <input type="text" class="form-control" id="adress" name = "adress" placeholder="enter adress">
  </div>
  
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" class="form-control" id="email" name = "email" placeholder="enter email">
  </div>
  <button type="submit" class="btn btn-default">save</button>
  <a href="annonces" class="btn btn-success">back</a>
</form>
</div>


</body>
</html>